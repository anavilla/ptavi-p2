#!/usr/bin/python3
# -*- coding: utf-8 -*-


class Calculadora():
    def __init__(self, operando1, operando2):
        self.operando1 = operando1
        self.operando2 = operando2

    def plus(self):
        return self.operando1 + self.operando2

    def minus(self):
        return self.operando1 - self.operando2


class CalculadoraHija(Calculadora):
    def mult(self):
        return self.operando1 * self.operando2

    def div(self):
        if self.operando2 == 0:
            print("Division by zero is not allowed")
        else:
            return self.operando1 / self.operando2


if __name__ == "__main__":

    fich = open("practica2fichero")

    for linea in fich:
        lista = linea.split(",")
        result = int(lista[1])

        if lista[0] == "suma":

            for operando2 in lista[2:]:
                result = CalculadoraHija(result, int(operando2)).plus()
            print(result)

        elif lista[0] == "resta":

            for operando2 in lista[2:]:
                result = CalculadoraHija(result, int(operando2)).minus()
            print(result)

        elif lista[0] == "multiplica":

            for operando2 in lista[2:]:
                result = CalculadoraHija(result, int(operando2)).mult()
            print(result)

        elif lista[0] == "divide":

            for operando2 in lista[2:]:
                result = CalculadoraHija(result, int(operando2)).div()
            print(result)

        else:
            sys.exit('Operación sólo puede ser sumar, '
                     'restar,multiplicar o dividir.')
